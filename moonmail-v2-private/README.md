# MoonMail-private

## Getting started

### Requirements
- yarn ([Install instructions](https://yarnpkg.com/en/docs/install))
- serverless [Serverless Framework](https://github.com/serverless/serverless)

**Notes about the serverless version**

Version **0.5.x** is required to run several parts of the MoonMail such as the **api** and **events** inside core and also in the public module, so this project fully depends on http://microapps.github.io/MoonMail/ for more details visit its [README](https://github.com/microapps/moonmail).

The rest for the services require serverless **1.x** and they are self-contained, for details on how to manage them you should follow the instructions in their respective README.md

### Dependencies

Install serverless 0.5 globally:
```
yarn global add serverless@0.5.6
```

MoonMail
```
github.com:microapps/MoonMail
```

### Bootrapping the project

1- Clone the `MoonMail` public repo:
```
git clone git@github.com:microapps/MoonMail.git
```

2- Cd into it and clone `Moonmail-private` (due to serverless 0.5 requirements, while we still depend on it we should follow this approach)
```
cd Moonmail && git clone git@bitbucket.org:micro-apps/moonmail-v2-private.git private
```

3- Follow `Moonmail public's` installation instructions

4- Cd into `private` and install dependencies

5- Install configurations inside _meta or any other serverless 1.0 microservice such as `template-marketplace`, etc 